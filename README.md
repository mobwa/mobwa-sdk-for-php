# OpenAPIClient-php

Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It 
currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD)



## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$create_account_request = new \OpenPDS\Model\CreateAccountRequest(); // \OpenPDS\Model\CreateAccountRequest

try {
    $result = $apiInstance->createAccount($create_account_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->createAccount: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountsApi* | [**createAccount**](docs/Api/AccountsApi.md#createaccount) | **POST** /accounts | Create an account
*AccountsApi* | [**getAccount**](docs/Api/AccountsApi.md#getaccount) | **GET** /accounts/{accountId} | Get account information
*AccountsApi* | [**listAccount**](docs/Api/AccountsApi.md#listaccount) | **GET** /accounts | List all accounts
*AccountsApi* | [**rechargeAccount**](docs/Api/AccountsApi.md#rechargeaccount) | **POST** /accounts/{accountId}/recharge | Recharge the account
*AccountsApi* | [**updateAccount**](docs/Api/AccountsApi.md#updateaccount) | **PUT** /accounts/{accountId} | Update account information
*AccountsApi* | [**withdrawMoney**](docs/Api/AccountsApi.md#withdrawmoney) | **POST** /accounts/{accountId}/withdraw | Withdraw money from the account
*SignupApi* | [**signup**](docs/Api/SignupApi.md#signup) | **POST** /signup | Signup
*TransfersApi* | [**completeTransfer**](docs/Api/TransfersApi.md#completetransfer) | **POST** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
*TransfersApi* | [**createTransfer**](docs/Api/TransfersApi.md#createtransfer) | **POST** /accounts/{accountId}/transfers | Create a transfer
*TransfersApi* | [**deleteTransfer**](docs/Api/TransfersApi.md#deletetransfer) | **DELETE** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
*TransfersApi* | [**getTransfer**](docs/Api/TransfersApi.md#gettransfer) | **GET** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
*TransfersApi* | [**listTransfers**](docs/Api/TransfersApi.md#listtransfers) | **GET** /accounts/{accountId}/transfers | List all transfers related to the account
*TransfersApi* | [**updateTransfer**](docs/Api/TransfersApi.md#updatetransfer) | **PUT** /accounts/{accountId}/transfers/{transferId} | Change transfer information
*UsersApi* | [**createUser**](docs/Api/UsersApi.md#createuser) | **POST** /users | Create a user
*UsersApi* | [**getUser**](docs/Api/UsersApi.md#getuser) | **GET** /users/{userId} | Get user information
*UsersApi* | [**listUsers**](docs/Api/UsersApi.md#listusers) | **GET** /users | List all users
*UsersApi* | [**updateUser**](docs/Api/UsersApi.md#updateuser) | **PUT** /users/{userId} | Update user information

## Models

- [Account](docs/Model/Account.md)
- [CreateAccountRequest](docs/Model/CreateAccountRequest.md)
- [CreateTransferRequest](docs/Model/CreateTransferRequest.md)
- [CreateUserRequest](docs/Model/CreateUserRequest.md)
- [Error](docs/Model/Error.md)
- [RechargeAccountRequest](docs/Model/RechargeAccountRequest.md)
- [SignUpRequest](docs/Model/SignUpRequest.md)
- [SignUpResponse](docs/Model/SignUpResponse.md)
- [Transfer](docs/Model/Transfer.md)
- [TransferSource](docs/Model/TransferSource.md)
- [UpdateAccountRequest](docs/Model/UpdateAccountRequest.md)
- [UpdateTransferRequest](docs/Model/UpdateTransferRequest.md)
- [User](docs/Model/User.md)

## Authorization

### basicAuth

- **Type**: HTTP basic authentication

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.0.0`
    - Package version: `camelCase`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
