# # SignUpResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **string** |  | [optional]
**email** | **string** |  | [optional]
**role** | **string** |  | [optional]
**accounts** | [**\OpenPDS\Model\Account[]**](Account.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
