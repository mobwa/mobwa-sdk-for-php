# # CreateTransferRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **string** |  | [optional]
**amount** | **int** |  | [optional]
**currency** | **string** |  | [optional]
**auto_complete** | **bool** |  | [optional]
**destination** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
