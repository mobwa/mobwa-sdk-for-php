# # Transfer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional]
**message** | **string** |  | [optional]
**amount** | **int** |  | [optional]
**currency** | **string** |  | [optional]
**created_at** | **string** |  | [optional]
**updated_at** | **string** |  | [optional]
**status** | **string** |  | [optional]
**auto_complete** | **bool** |  | [optional]
**source** | [**\OpenPDS\Model\TransferSource**](TransferSource.md) |  | [optional]
**destination** | [**\OpenPDS\Model\TransferSource**](TransferSource.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
