# OpenPDS\AccountsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAccount()**](AccountsApi.md#createAccount) | **POST** /accounts | Create an account
[**getAccount()**](AccountsApi.md#getAccount) | **GET** /accounts/{accountId} | Get account information
[**listAccount()**](AccountsApi.md#listAccount) | **GET** /accounts | List all accounts
[**rechargeAccount()**](AccountsApi.md#rechargeAccount) | **POST** /accounts/{accountId}/recharge | Recharge the account
[**updateAccount()**](AccountsApi.md#updateAccount) | **PUT** /accounts/{accountId} | Update account information
[**withdrawMoney()**](AccountsApi.md#withdrawMoney) | **POST** /accounts/{accountId}/withdraw | Withdraw money from the account


## `createAccount()`

```php
createAccount($create_account_request): \OpenPDS\Model\Transfer[]
```

Create an account

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$create_account_request = new \OpenPDS\Model\CreateAccountRequest(); // \OpenPDS\Model\CreateAccountRequest

try {
    $result = $apiInstance->createAccount($create_account_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->createAccount: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_account_request** | [**\OpenPDS\Model\CreateAccountRequest**](../Model/CreateAccountRequest.md)|  | [optional]

### Return type

[**\OpenPDS\Model\Transfer[]**](../Model/Transfer.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getAccount()`

```php
getAccount($account_id): \OpenPDS\Model\Account
```

Get account information

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_id = 'account_id_example'; // string | The id of the account

try {
    $result = $apiInstance->getAccount($account_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->getAccount: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account |

### Return type

[**\OpenPDS\Model\Account**](../Model/Account.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `listAccount()`

```php
listAccount(): \OpenPDS\Model\Account[]
```

List all accounts

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->listAccount();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->listAccount: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenPDS\Model\Account[]**](../Model/Account.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rechargeAccount()`

```php
rechargeAccount($account_id, $recharge_account_request): \OpenPDS\Model\Account
```

Recharge the account

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_id = 'account_id_example'; // string | The id of the account
$recharge_account_request = new \OpenPDS\Model\RechargeAccountRequest(); // \OpenPDS\Model\RechargeAccountRequest

try {
    $result = $apiInstance->rechargeAccount($account_id, $recharge_account_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->rechargeAccount: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account |
 **recharge_account_request** | [**\OpenPDS\Model\RechargeAccountRequest**](../Model/RechargeAccountRequest.md)|  | [optional]

### Return type

[**\OpenPDS\Model\Account**](../Model/Account.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateAccount()`

```php
updateAccount($account_id, $update_account_request): \OpenPDS\Model\Account
```

Update account information

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_id = 'account_id_example'; // string | The id of the account
$update_account_request = new \OpenPDS\Model\UpdateAccountRequest(); // \OpenPDS\Model\UpdateAccountRequest

try {
    $result = $apiInstance->updateAccount($account_id, $update_account_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->updateAccount: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account |
 **update_account_request** | [**\OpenPDS\Model\UpdateAccountRequest**](../Model/UpdateAccountRequest.md)|  | [optional]

### Return type

[**\OpenPDS\Model\Account**](../Model/Account.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `withdrawMoney()`

```php
withdrawMoney($account_id, $body): \OpenPDS\Model\Account
```

Withdraw money from the account

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_id = 'account_id_example'; // string | The id of the account
$body = new \OpenPDS\Model\RechargeAccountRequest(); // \OpenPDS\Model\RechargeAccountRequest

try {
    $result = $apiInstance->withdrawMoney($account_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->withdrawMoney: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account |
 **body** | **\OpenPDS\Model\RechargeAccountRequest**|  | [optional]

### Return type

[**\OpenPDS\Model\Account**](../Model/Account.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
