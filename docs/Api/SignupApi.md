# OpenPDS\SignupApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**signup()**](SignupApi.md#signup) | **POST** /signup | Signup


## `signup()`

```php
signup($sign_up_request): \OpenPDS\Model\SignUpResponse
```

Signup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenPDS\Api\SignupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sign_up_request = new \OpenPDS\Model\SignUpRequest(); // \OpenPDS\Model\SignUpRequest

try {
    $result = $apiInstance->signup($sign_up_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SignupApi->signup: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sign_up_request** | [**\OpenPDS\Model\SignUpRequest**](../Model/SignUpRequest.md)|  | [optional]

### Return type

[**\OpenPDS\Model\SignUpResponse**](../Model/SignUpResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
