# OpenPDS\TransfersApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**completeTransfer()**](TransfersApi.md#completeTransfer) | **POST** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
[**createTransfer()**](TransfersApi.md#createTransfer) | **POST** /accounts/{accountId}/transfers | Create a transfer
[**deleteTransfer()**](TransfersApi.md#deleteTransfer) | **DELETE** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
[**getTransfer()**](TransfersApi.md#getTransfer) | **GET** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
[**listTransfers()**](TransfersApi.md#listTransfers) | **GET** /accounts/{accountId}/transfers | List all transfers related to the account
[**updateTransfer()**](TransfersApi.md#updateTransfer) | **PUT** /accounts/{accountId}/transfers/{transferId} | Change transfer information


## `completeTransfer()`

```php
completeTransfer($account_id, $transfer_id): \OpenPDS\Model\Transfer
```

Complete a transfer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\TransfersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_id = 'account_id_example'; // string | The id of the account
$transfer_id = 'transfer_id_example'; // string | The id of the transfer to operate on

try {
    $result = $apiInstance->completeTransfer($account_id, $transfer_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransfersApi->completeTransfer: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account |
 **transfer_id** | **string**| The id of the transfer to operate on |

### Return type

[**\OpenPDS\Model\Transfer**](../Model/Transfer.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `createTransfer()`

```php
createTransfer($account_id, $create_transfer_request): \OpenPDS\Model\Transfer
```

Create a transfer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\TransfersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_id = 'account_id_example'; // string | The id of the account
$create_transfer_request = new \OpenPDS\Model\CreateTransferRequest(); // \OpenPDS\Model\CreateTransferRequest

try {
    $result = $apiInstance->createTransfer($account_id, $create_transfer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransfersApi->createTransfer: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account |
 **create_transfer_request** | [**\OpenPDS\Model\CreateTransferRequest**](../Model/CreateTransferRequest.md)|  | [optional]

### Return type

[**\OpenPDS\Model\Transfer**](../Model/Transfer.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteTransfer()`

```php
deleteTransfer($account_id, $transfer_id): \OpenPDS\Model\Transfer
```

Delete a transfer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\TransfersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_id = 'account_id_example'; // string | The id of the account
$transfer_id = 'transfer_id_example'; // string | The id of the transfers to operate on

try {
    $result = $apiInstance->deleteTransfer($account_id, $transfer_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransfersApi->deleteTransfer: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account |
 **transfer_id** | **string**| The id of the transfers to operate on |

### Return type

[**\OpenPDS\Model\Transfer**](../Model/Transfer.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getTransfer()`

```php
getTransfer($account_id, $transfer_id): \OpenPDS\Model\Transfer
```

Retrieve information for a specific transfer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\TransfersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_id = 'account_id_example'; // string | The id of the account
$transfer_id = 'transfer_id_example'; // string | The id of the transfers to operate on

try {
    $result = $apiInstance->getTransfer($account_id, $transfer_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransfersApi->getTransfer: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account |
 **transfer_id** | **string**| The id of the transfers to operate on |

### Return type

[**\OpenPDS\Model\Transfer**](../Model/Transfer.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `listTransfers()`

```php
listTransfers($account_id): \OpenPDS\Model\Transfer[]
```

List all transfers related to the account

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\TransfersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_id = 'account_id_example'; // string | The id of the account

try {
    $result = $apiInstance->listTransfers($account_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransfersApi->listTransfers: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account |

### Return type

[**\OpenPDS\Model\Transfer[]**](../Model/Transfer.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateTransfer()`

```php
updateTransfer($account_id, $transfer_id, $update_transfer_request): \OpenPDS\Model\Transfer
```

Change transfer information

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = OpenPDS\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenPDS\Api\TransfersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_id = 'account_id_example'; // string | The id of the account
$transfer_id = 'transfer_id_example'; // string | The id of the transfers to operate on
$update_transfer_request = new \OpenPDS\Model\UpdateTransferRequest(); // \OpenPDS\Model\UpdateTransferRequest

try {
    $result = $apiInstance->updateTransfer($account_id, $transfer_id, $update_transfer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransfersApi->updateTransfer: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account |
 **transfer_id** | **string**| The id of the transfers to operate on |
 **update_transfer_request** | [**\OpenPDS\Model\UpdateTransferRequest**](../Model/UpdateTransferRequest.md)|  | [optional]

### Return type

[**\OpenPDS\Model\Transfer**](../Model/Transfer.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
